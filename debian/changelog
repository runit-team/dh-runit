dh-runit (2.8.11) experimental; urgency=medium

  * Make build system respect `nocheck' option.
  * Create perl package for test-related code.
  * [cb411aff] Add option to mark service as non-restartable
  * [8eddb040] Make it possible to reuse t/*.t for autopkgtest
  * [e4b8e832] Refactor creation of logscript
  * [4c485bc0] Create marker files for use by "invoke-run"
  * [2b3d1cb0] Add Gitlab CI config file
  * [32409bf7] Add autopkgtests

 -- Dmitry Bogatov <KAction@debian.org>  Wed, 29 May 2019 06:54:45 +0000

dh-runit (2.8.10) experimental; urgency=medium

  * Make permissions of supervise directories 0700, as created by sv(8).
    (Closes: #924903)
  * Run testsuite during package build.

 -- Dmitry Bogatov <KAction@debian.org>  Wed, 20 Mar 2019 13:17:15 +0000

dh-runit (2.8.9) experimental; urgency=medium

  * Fix missing directory error when installing runscript directory.

 -- Dmitry Bogatov <KAction@debian.org>  Tue, 12 Mar 2019 20:42:43 +0000

dh-runit (2.8.8) experimental; urgency=medium

  * Do not create /etc/sv directory if there is no runscript (Closes: #923439)
    + Thanks: Mathieu Mirmont <mat@parad0x.org>
  * Correctly create symlinks in /etc/service in post-inst on first
    revision of package, that introduced runscript. (Closes: #923233)

 -- Dmitry Bogatov <KAction@debian.org>  Tue, 05 Mar 2019 15:25:39 +0000

dh-runit (2.8.7) experimental; urgency=medium

  * Generate breaks with runit version, predating 'invoke-run' script.

 -- Dmitry Bogatov <KAction@debian.org>  Sun, 24 Feb 2019 20:56:42 +0000

dh-runit (2.8.6) unstable; urgency=medium

  * Fix short description of runit-helper (Closes: #922413)

 -- Dmitry Bogatov <KAction@debian.org>  Fri, 15 Feb 2019 17:56:50 +0000

dh-runit (2.8.5) unstable; urgency=medium

  * Mark `runit-helper' as Multi-Arch: foreign (Closes: #921950)
    + Thanks: Elrond <elrond+bugs.debian.org@samba-tng.org>

 -- Dmitry Bogatov <KAction@debian.org>  Sun, 10 Feb 2019 19:58:35 +0000

dh-runit (2.8.4) unstable; urgency=medium

  * Copy-edit dh_runit(1) documentation. (Closes: #920963)
    + Thanks: Jonathan Nieder <jrnieder@gmail.com>

 -- Dmitry Bogatov <KAction@debian.org>  Sun, 03 Feb 2019 23:19:55 +0000

dh-runit (2.8.3) unstable; urgency=medium

  * Document runit:Breaks substitution variable

 -- Dmitry Bogatov <KAction@debian.org>  Tue, 22 Jan 2019 22:09:24 +0000

dh-runit (2.8.2) unstable; urgency=medium

  * Upload to unstable
  * Update Maintainer field
  * Bump debhelper-compat to 12
  * Update standards version to 4.3.0 (no changes needed)

 -- Dmitry Bogatov <KAction@debian.org>  Sat, 19 Jan 2019 03:21:35 +0000

dh-runit (2.8.1) experimental; urgency=medium

  * Make auto-generated runscripts invoke svlogd(8) as `runit-log' user.
  * Impose dependency on (runit >= 2.1.2-20), which provides `runit-log'
    user.
  * Do not impose dependency on `runit' binary package. Instead,
    generate conflict relation with old `runit' version, not providing
    `runit-log' user.
  * Make `/etc/runit/runsvdir/default' directory part of package,
    providing runscript.

 -- Dmitry Bogatov <KAction@debian.org>  Sun, 16 Dec 2018 10:16:30 +0000

dh-runit (2.7.3) unstable; urgency=medium

  * Do not re-enable serice on upgrade, if it was disable by local
    administrator (See #899242)

 -- Dmitry Bogatov <KAction@gnu.org>  Wed, 23 May 2018 08:19:55 +0300

dh-runit (2.7.2) unstable; urgency=medium

  * Invoke runit-helper only if it is available. It ensures, that in
    pathological case, when it isn't (piuparts), postrm script do not fail.
  * Bump compat version to 11 (no changes needed)
  * Update standards version to 4.1.4 (no changes needed)

 -- Dmitry Bogatov <KAction@gnu.org>  Sat, 14 Apr 2018 17:58:58 +0300

dh-runit (2.7.1) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Bogatov <KAction@gnu.org>  Sun, 11 Mar 2018 12:11:26 +0300

dh-runit (2.7) experimental; urgency=medium

  * Change naming of generated system users for logging to start
    with underscore.
  * Add missing dependency on dh-sysuser.
  * Improve readability of code, generating /etc/sv/<daemon>/log/run
    script using here-doc notation.
  * Remove 'logdir' option for simplicity sake. Strictly speaking it is
    backward-incompatible change, hence the version major bump, but I am
    unaware of any actual users of this option.
  * Ensure that after package removal logs belong to root.
  * Introduce new binary package 'runit-helper', allowing packages to
    access fixes and improvements in 'dh-runit' without rebuild.
  * Update Vcs-* and Homepage fields in debian/control.

 -- Dmitry Bogatov <KAction@gnu.org>  Wed, 07 Mar 2018 03:33:22 +0300

dh-runit (1.6.2) unstable; urgency=medium

  * Remove from 'dh_runit' code to install no longer present prerm
    maintainer script.

 -- Dmitry Bogatov <KAction@gnu.org>  Wed, 14 Sep 2016 12:23:17 +0300

dh-runit (1.6.1) unstable; urgency=medium

  * Upload to unstable

 -- Dmitry Bogatov <KAction@gnu.org>  Sun, 11 Sep 2016 21:53:47 +0300

dh-runit (1.6) experimental; urgency=medium

  * Do not create /etc/runit/runsvdir/default directory. Instead, it is
    provided by runit binary package. It saved us trouble of ensuring,
    that directory is removed at apporiate time.
  * Do not create /etc/service symbolic link in maintainer script of
    -run packages. It simplifies maintainace at cost of creation
    of empty directories in runit binary package. But since it is rare
    to install runit, but none of -run packages, trade-off seems justified.
  * Do not create symbolic link in /etc/runit/runsvdir/ in maintainer script,
    make it part of binary package. It makes sure, that dpkg will make right
    thing.
  * No need to invoke force-shutdown in maintainer script, runit will notice
    disappeared link anyway.

 -- Dmitry Bogatov <KAction@gnu.org>  Sat, 03 Sep 2016 20:52:18 +0300

dh-runit (1.5) unstable; urgency=medium

  * Breaking change package.runit file format to improve interoperability
    with other tools (supply dh_runit arguments on command line) and make
    format extensible. See dh_runit(1).
  * Introduce support for automatic generation of log scripts.
  * Fix typo in mkdir option.
  * Force shutdown of service we are uninstalling. Otherwise, it would be
    5 seconds gap, when process are still alive.
  * Clean-up supervision directory of log process too.

 -- Dmitry Bogatov <KAction@gnu.org>  Tue, 23 Aug 2016 12:59:59 +0300

dh-runit (0.4) unstable; urgency=medium

  * Fix bug, that caused dh-runit to attempt to create directory
    under /.
  * Fix postrm script. Previously, it halted purge with errors about
    missing supervise directory. Indeed, if service was never started,
    it's supervise directory is empty and removed by dpkg.

 -- Dmitry Bogatov <KAction@gnu.org>  Thu, 28 Jul 2016 18:46:49 +0300

dh-runit (0.3) unstable; urgency=medium

  * Support for `runlevels'. Now, runscripts are installed for
    `runlevel' default, but system adminstrator can create more.

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 15 Jul 2016 09:45:08 +0300

dh-runit (0.2) unstable; urgency=medium

  * Change arch:any to arch:all. Perl script is arch-independent.

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 15 Jul 2016 09:45:04 +0300

dh-runit (0.1) unstable; urgency=medium

  * Initial release (Closes: #826771)

 -- Dmitry Bogatov <KAction@gnu.org>  Sat, 04 Jun 2016 13:41:17 +0300
