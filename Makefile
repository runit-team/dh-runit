all:
check:
	PATH=$(CURDIR):$(PATH) \
	DH_AUTOSCRIPTDIR=$(CURDIR) \
	DH_RUNIT_DATADIR=$(CURDIR)/data \
	prove -I.
autopkgtest:
	prove -I.
.PHONY: check
