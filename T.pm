package T;
use strict;
use warnings;
use Cwd;
use Test::More;
use File::Copy::Recursive qw(dircopy);
use File::Path qw(remove_tree);
use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(system_ok);

my $root = Cwd::cwd;

die '$0 does not match expected format'
    unless ($0 =~ m#t/([0-9]+)\.t#);
my $number = $1;
my $srcdir = (-d "t/base/${number}") ? "t/base/${number}" : "t/base/default";
my $testdir = "t/${number}";
remove_tree($testdir);
dircopy($srcdir, $testdir);
chdir($testdir);

sub system_ok { ok(system(@_) == 0, "external command @_");}

1;
