#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 4;
use T;

system_ok('dh_runit', 'debian/test.runscript', 'name=test,noreplace,logscript');

my $noreplace = 'debian/dh-runit-test/usr/share/runit/meta/test/noreplace';
ok(-f $noreplace, 'noreplace file correctly created');
my $logscript = 'debian/dh-runit-test/etc/sv/test/log/run';
ok(-f $logscript, 'logscript correctly created');
ok(-x $logscript, 'logscript is executable');
