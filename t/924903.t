#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 4;
use File::stat;
use T;

system_ok('dh_runit', 'debian/test.runscript', 'name=test');
my $path = 'debian/dh-runit-test/var/lib/runit/supervise/test';
ok(-d $path, 'supervise directory correctly created');
my $info = stat($path);
my $mode = sprintf("%o", $info->mode & 0777);
is($mode, '700', 'supervise directory have conservative permissions');

my $noreplace = 'debian/dh-runit-test/var/lib/runit/noreplace/test';
ok(!-f $noreplace, 'noreplace file is correctly absent');
